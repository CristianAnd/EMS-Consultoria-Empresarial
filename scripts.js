document.addEventListener('DOMContentLoaded', function () {
    var carouselItems = document.querySelectorAll('.carousel-item');
    var currentSlide = 0;
  
    function showSlide(index) {
      if (index >= carouselItems.length) {
        index = 0;
      } else if (index < 0) {
        index = carouselItems.length - 1;
      }
  
      // Esconde todos os slides
      carouselItems.forEach(function (slide) {
        slide.classList.remove('active');
      });
  
      // Mostra o slide atual
      carouselItems[index].classList.add('active');
      currentSlide = index;
    }
  
    // Função para avançar o slide
    function nextSlide() {
      showSlide(currentSlide + 1);
    }
  
    // Função para retroceder o slide
    function prevSlide() {
      showSlide(currentSlide - 1);
    }
  
    // Inicia o carrossel
    showSlide(currentSlide);
  
    // Configura os botões para avançar e retroceder os slides
    document.querySelector('.carousel').addEventListener('click', function (event) {
      if (event.target.classList.contains('next')) {
        nextSlide();
      } else if (event.target.classList.contains('prev')) {
        prevSlide();
      }
    });
  
    // Define o intervalo para mudar automaticamente os slides
    setInterval(nextSlide, 10000000000);
  });


  AOS.init();

  